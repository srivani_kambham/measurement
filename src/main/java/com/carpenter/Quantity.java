package com.carpenter;

import java.util.Objects;

public class Quantity {

    double value;
    Unit unit;

    private Quantity(double value, Unit unit)
    {
        this.value = value;
        this.unit = unit;
    }

    public static Quantity Kilometer(double value) {
        return new Quantity(value=value, Unit.KILOMETER);
    }

    public static Quantity Meter(double value) {
        return new Quantity(value=value, Unit.METER);
    }

    public static Quantity Centimeter(double value) {
        return new Quantity(value=value, Unit.CENTIMETER);
    }

    public static Quantity Kilogram(double value) {
        return new Quantity(value=value, Unit.KILOGRAM);
    }

    public static Quantity Gram(double value) {
        return new Quantity(value=value, Unit.GRAM);
    }


    @Override
    public boolean equals(Object o1) {
        if(this==o1) return true;
        if (o1 == null || getClass() != o1.getClass()) return false;
        Quantity quantity = (Quantity) o1;
        if(this.unit.isSameType(quantity.unit))
            return Double.compare(this.unit.convertToBase(this.value, quantity.unit), quantity.value) == 0;

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, unit);
    }



}
