package com.carpenter;

import java.util.Objects;

public class Temperature {
    double value;
    String unit;
    public Temperature(double value, String unit) {
        this.value=value;
        this.unit=unit;
    }
    double convertToFahrenheit(double value,String unit){
       double result=0;
      switch(unit)
      {
          case "C":result=(value*1.8)+32;break;
          case "F":result=value;break;
          case "K":result=(value*1.8)-459.67;break;

      }
          return Math.round(result);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Temperature that = (Temperature) o;
        return Double.compare(convertToFahrenheit(that.value, that.unit),convertToFahrenheit(value,unit))==0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, unit);
    }
}
