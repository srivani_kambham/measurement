package com.carpenter;

public class Unit {
    private UnitType unitType;
    private double factor;
    private static final double ONE = 1.0;
    private static final double KILO = 1000;
    private static final double CENTI = 0.01;

    private Unit(UnitType unitType, double factor){
        this.unitType = unitType;
        this.factor = factor;
    }

    public static final Unit KILOMETER= new Unit(UnitType.LENGTH, KILO);
    public static final Unit METER= new Unit(UnitType.LENGTH, ONE);
    public static final Unit CENTIMETER= new Unit(UnitType.LENGTH, CENTI);
    public static final Unit KILOGRAM = new Unit(UnitType.MASS,KILO);
    public static final  Unit GRAM = new Unit(UnitType.MASS,ONE);

    boolean isSameType(Unit unit) {
        return this.unitType.equals(unit.unitType);
    }

    Double convertToBase(double value,Unit unit) {
        return (this.factor * value)/ unit.factor;
    }

}

