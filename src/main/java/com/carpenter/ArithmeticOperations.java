package com.carpenter;

public class ArithmeticOperations {
    private double value;
    private Unit unit;
    public ArithmeticOperations(double value, Unit unit) {
        this.value = value;
        this.unit = unit;
    }

    public double addQuantities(ArithmeticOperations aq) {
        return this.value+aq.unit.convertToBase(aq.value,this.unit);
    }

    public double subtractQuantities(ArithmeticOperations aq) {
        return this.value-aq.unit.convertToBase(aq.value,this.unit);
    }


}
