package com.carpenter;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UnitTest {
    @Test
    public void shouldCheckTypeEquality(){
        assertEquals(true, Unit.CENTIMETER.isSameType(Unit.METER));
    }

    @Test
    public void shouldConvertToAnotherUnit(){
        assertEquals(1.0, Unit.CENTIMETER.convertToBase(100.0,Unit.METER));
    }

}
