package com.carpenter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ArithmeticOperationsTest {
    @Test
    public void shouldAddQuantitiesOfCmandM() {
        Assertions.assertEquals(200,new ArithmeticOperations(100,Unit.CENTIMETER).addQuantities(new ArithmeticOperations(1,Unit.METER)));
    }
    @Test
    public void shouldAddQuantitiesOOfKmAndM() {
        Assertions.assertEquals(1.1,new ArithmeticOperations(1,Unit.KILOMETER).addQuantities(new ArithmeticOperations(100,Unit.METER)));

    }
    @Test
    public void shouldAddDifferenceOfKmAndM() {
        Assertions.assertEquals(0.9, new ArithmeticOperations(1,Unit.KILOMETER).subtractQuantities(new ArithmeticOperations(100,Unit.METER)));
    }

}
