package com.carpenter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class QuantityTest {

    @Test
    void shouldCheckEqualityForKmToM()
    {

        Assertions.assertEquals(Quantity.Kilometer(1), Quantity.Meter(1000));

    }
    @Test
    void shouldCheckInequalityForMToKM()
    {

        Assertions.assertNotEquals(Quantity.Kilometer(1), Quantity.Meter(10));

    }

    @Test
    void shouldCheckEqualityForCmToM()
    {

        Assertions.assertEquals(Quantity.Centimeter(100), Quantity.Meter(1));

    }


    @Test
    void shouldCheckEqualityForKmToCm()
    {
        Assertions.assertEquals(Quantity.Kilometer(1), Quantity.Centimeter(100000));

    }

    @Test
    void shouldCheckEqualityForCmToKm()
    {
        Assertions.assertEquals(Quantity.Centimeter(100000), Quantity.Kilometer(1));

    }

    @Test
    void shouldCheckEqualityForKgToG()
    {
        Assertions.assertEquals(Quantity.Kilogram(1), Quantity.Gram(1000));

    }

    @Test
    void shouldCheckEqualityForGToKg()
    {
        Assertions.assertEquals(Quantity.Gram(2100), Quantity.Kilogram(2.1));
    }


}
