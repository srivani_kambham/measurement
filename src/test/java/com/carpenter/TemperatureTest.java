package com.carpenter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TemperatureTest {
    @Test
    void shouldCheckEqualityOfTemperaturesForCTOF()
    {
        Assertions.assertEquals(new Temperature(25,"C"),new Temperature(77,"F"));
    }
    @Test
    void shouldCheckEqualityOfTemperaturesForFToC()
    {
        Assertions.assertEquals(new Temperature(91.4,"F"),new Temperature(33,"C"));
    }
    @Test
    void shouldCheckEqualityOfTemperaturesForFToK()
    {
        Assertions.assertEquals(new Temperature(10,"F"),new Temperature(260.93,"K"));
    }
    @Test
    void shouldCheckEqualityOfTemperaturesForKToF()
    {
        Assertions.assertEquals(new Temperature(227.59,"K"),new Temperature(-50,"F"));
    }
    @Test
    void shouldCheckEqualityOfTemperaturesForCToK()
    {
        Assertions.assertEquals(new Temperature(10,"C"),new Temperature(283.15,"K"));
    }
    @Test
    void shouldCheckEqualityOfTemperaturesForKToC()
    {
        Assertions.assertEquals(new Temperature(293.15,"K"),new Temperature(20,"C"));
    }


}
